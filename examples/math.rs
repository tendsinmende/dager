use std::sync::Arc;
use std::sync::Mutex;

use dager::edge::Edge;
use dager::executor::Executor;
use dager::node::AbstAggregator;
use dager::node::Aggregator;
use dager::node::Node;

struct Add;
impl Node for Add{
    type InSig = (f32, f32);
    type OutSig = [f32; 1];
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let (a,  b) = input;
	println!("adding: {}+{}", a, b);
	[a + b]
    }

    fn default(&mut self, aggregator: &mut dyn AbstAggregator){
	println!("Setting default of port 0 to 1");
	aggregator.set_default_value(0, Box::new(1.0 as f32)).expect("Failed to set default");
    }

    fn name<'a>(&'a self) -> &'a str {
	"Adder"
    }
}

struct Printer;
impl Node for Printer{
    type InSig = [f32; 1];
    type OutSig = [f32; 1];
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	let [a] = input;
	println!("Got {}", a);
	[0.0]
    }

    fn name<'a>(&'a self) -> &'a str {
	"Printer"
    }
}


//Note graph:
//     default 1.0 -|add1|--\   1.0 -|add2| ----|printer|
//  from main 2.0  -|    |   \-------|    |     |       |
//
//
fn main(){
    let ex = Executor::new();
    
    let add_node1 = Arc::new(Mutex::new(Aggregator::from_node(Add)));
    let add_node2 = Arc::new(Mutex::new(Aggregator::from_node(Add)));
    let printer = Arc::new(Mutex::new(Aggregator::from_node(Printer)));
    
    Edge::connect(add_node1.clone(), 0, add_node2.clone(), 1).expect("Failed to connect edge");
    Edge::connect(add_node2.clone(), 0, printer.clone(), 0).expect("Failed to connect edge");
    
    //Should execute since the other edge is set by the default value
    add_node1.lock().unwrap().set_in_from_edge(ex.clone(), 1, Box::new(2.0 as f32)).expect("Failed to set input");

    std::thread::sleep(std::time::Duration::from_secs(1));
}
