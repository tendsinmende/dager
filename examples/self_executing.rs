//! Creates a graph with some object, that executes it self periodically in some other thread.
use dager::{arc_node, Node, Executor, Edge, node::Executable};

struct MuchWork;
impl Node for MuchWork{
    type InSig = (i32, String);
    type OutSig = [usize; 1];
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	println!("{} doing much work at {}", input.1, input.0);
	std::thread::sleep(std::time::Duration::from_secs_f32(0.5));
	println!("Send");
	[input.0 as usize]
    }
}

struct Printer;
impl Node for Printer{
    type InSig = [usize; 1];
    type OutSig = ();
    fn process(&mut self, input: Self::InSig) -> Self::OutSig {
	println!("Printer got: {}", input[0]);
    }
}

struct SelfExecutor{
    inner_state: i32,
}

impl Node for SelfExecutor{
    type InSig = ();
    type OutSig = (i32, String);
    fn process(&mut self, _input: Self::InSig) -> Self::OutSig {
	self.inner_state += 1;
	(self.inner_state, format!("Worker[{}]", self.inner_state))
    }
}


//GRAPH
//  ________________   i32    ______________   usize  ___________
//  | SelfExecutor |----------|  MuchWork  |----------| Printer |
//  |              |  String  |            |          |_________|
//  |              |----------|            |
//  |______________|          |____________|
fn main(){
    let executor = Executor::new();
    let exec = arc_node(SelfExecutor{
	inner_state: 0,
    });

    let worker = arc_node(MuchWork);
    let printer = arc_node(Printer);

    Edge::connect(exec.clone(), 0, worker.clone(), 0).expect("Failed to connect");
    Edge::connect(exec.clone(), 1, worker.clone(), 1).expect("Failed to connect");

    Edge::connect(worker.clone(), 0, printer.clone(), 0).expect("Failed to connect");

    //Start another thread with the exec and tick it from time to time.
    let th = std::thread::spawn(move ||{
	//Tick every 2sec
	loop{
	    //Won't do anything since we don't have an input. However the execution will be started, since all edges are set.
	    exec.lock().unwrap().execute(executor.clone()).expect("Failed to execute start node");
	    std::thread::sleep(std::time::Duration::from_secs(1));
	}
    });


    th.join().unwrap();
}


