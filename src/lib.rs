/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


//! # Dager
//! Dager is the second (or third) attempt at creating an easy to use, graph execution library. The biggest advancment over `dagxecutor`
//! is its shorter name... and the ability to define default values for inputs of nodes.
//!
//! # Principle
//! ## Aggregators and Nodes
//! As an user you'll usually only have to implement the `Node` trait to have new node types.
//! A node can then be wrapped into an `Aggregator`. The aggregator handles the waiting for all required data, execution of your node
//! and sending of the processed data to other connected nodes.
//! 
//! ## Implementing a node
//! A node has three mandatory parts.
//! 1. an input signature
//! 2. an output signature
//! 3. a process function that takes an input signature and outputs the output signature
//!
//! The signatures should always be defined as tuples (excluding a single value which is [T;1], check the note on why that is in the signature mod).
//! On that node, implementing add for `f32` would look like this:
//!
//! ```
//! use dager::Node;
//! struct Add;
//! impl Node for Add{
//!     type InSig = (f32, f32);
//!     type OutSig = [f32;1];
//!     fn process(&mut self, input: Self::InSig) -> Self::OutSig{
//!         [input.0 + input.1]
//!     }
//! }
//! ```
//! ## Graphs and execution
//! A graph is defined by some `AbstAggregator`s that are connected via `Edge`s. One or several `Executor`s can be used to set
//! Inputs of those aggregators. If an aggregator has all its inputs set, either by the inner nodes `default()` implementations or in some other way,
//! the node within this aggregator is executed. The result is then send to connected aggregators.
//! Have a look at `examples/math.rs` for some small example.

///Small thread pool based executor that is used when scheduling new threads from within a node.
pub mod executor;
pub use executor::Executor;
///Defines the interface a node must provide to work with this graph.
pub mod node;
pub use node::{arc_node, Node, AbstAggregator, Aggregator, Executable};

///Defines how edges are set between nodes
pub mod edge;
pub use edge::Edge;
///Contains definitions of data collection for in and output ports
pub mod collections;

///Contains the description of input ports as well as the implementations of them for several tubel.
pub mod inport;

///Contains the description of output ports as well as the implementation of them for several tubel.
pub mod outport;

///Contains IoSignature related implementations as well as the main trait that need to be implemented for all of them.
pub mod signature;


///All runtime errors that can occure while executing or manipulating some graph.
#[derive(Clone, Copy, Debug)]
pub enum DErr{
    TypeMissmatch,
    NoSuchPort,
    ExecutorError(executor::ExecutorError),
    NoEdgeOnIdx,
    ///Occurs if an edge is removed where the partners connection do not match.
    SinglePartenerEdge,
}
