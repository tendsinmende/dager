/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */


use std::marker::PhantomData;

use crate::edge::Edge;

///represent one input pin. Might have a edge assigned. If not, the default value provided by the node is used. Otherwise
/// The aggregator waits for a input via an edge.
pub struct InCol<A>{
    pub edge: Option<Edge>,
    pub value: Option<A>
}

impl<A> Default for InCol<A>{
    fn default() -> Self{
	InCol{
	    edge: None,
	    value: None
	}
    }
}

///Represents some output pin that might have an edge attached. If an edge is attached, the parent node
/// can send data to some receiving node's aggregator.
pub struct OutCol<A>{
    ty: PhantomData<A>,
    pub edge: Option<Edge>,
}

impl<A> Default for OutCol<A>{
    fn default() -> Self {
	OutCol{
	    ty: PhantomData,
	    edge: None
	}
    }
}
